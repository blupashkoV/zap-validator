**Requirements:** 

- java 8
- maven 3 and above (for build only)

**Run**
```
java -jar app
```

**Example**:
```
java -jar validator-0.0.1-SNAPSHOT-jar-with-dependencies.jar --login https://prd-wlt-dk-01.fx.lan:8443/rest/public/api/security/login --logout https://prd-wlt-dk-01.fx.lan:8443/rest/api/security/logout --url https://prd-wlt-dk-01.fx.lan:8443/crm-solution --username ameliathomas50050001 --password Passw0rd! --riskLevel 2 --riskTimes 1 
```

**Build**
```
$ mvn clean compile assembly:single
```
