package com.thedigitalstack.zapproxy.validator.util;

import com.beust.jcommander.JCommander;
import com.beust.jcommander.Parameter;
import com.thedigitalstack.zapproxy.validator.exception.ZapValidatorException;
import com.thedigitalstack.zapproxy.validator.model.Args;

import java.util.Scanner;

public class ArgInitializer {

    private static final String ERROR_MSG = "Args should contain:  \n"
            + "login url for *.fx.lan \n"
            + "logout url for *.fx.lan \n"
            + "url for *.fx.lan \n"
            + "username \n"
            + "password \n"
            + "riskLevel \n"
            + "occur times \n"
            + "Example: \n"
            + "java -jar validator-0.0.1-SNAPSHOT-jar-with-dependencies.jar "
            + "--login https://prd-wlt-dk-01.fx.lan:8443/rest/public/api/security/login "
            + "--logout https://prd-wlt-dk-01.fx.lan:8443/rest/api/security/logout "
            + "--url https://prd-wlt-dk-01.fx.lan:8443/crm-solution "
            + "--username ameliathomas50050001 "
            + "--password Passw0rd! "
            + "--riskLevel 2 "
            + "--riskTimes 1 ";

    @Parameter(names = {"--riskLevel"})
    private Integer riskLevel;
    @Parameter(names = {"--riskTimes"})
    private Integer riskTimes;
    @Parameter(names = {"--login"})
    private String login;
    @Parameter(names = {"--logout"})
    private String logout;
    @Parameter(names = {"--url"})
    private String url;
    @Parameter(names = {"--username"})
    private String username;
    @Parameter(names = {"--password"})
    private String password;

    public static void main(String[] args) {
        System.out.println(ERROR_MSG);
    }

    public static Args validateAndInitializeArgs(String[] args) {

        if (args == null || args.length == 0) {
            return getArgsInteractive();
        }

        ArgInitializer initializer = new ArgInitializer();
        try {
            JCommander.newBuilder()
                    .addObject(initializer)
                    .build()
                    .parse(args);
        } catch (Exception ex) {
            throw new ZapValidatorException(ERROR_MSG);
        }

        return initializer.getArgsCli();
    }

    private Args getArgsCli() {
        if (login == null || logout == null || url == null || username == null || password == null || riskLevel == null || riskTimes == null) {
            throw new ZapValidatorException(ERROR_MSG);

        }

        return Args.builder()
                .loginUrl(login)
                .logoutUrl(logout)
                .url(url)
                .loggedInIndicator("<a _ngcontent-c1=\"\" href=\"javascript:void(0)\" translate=\"\"> Logout </a>")
                .username(username)
                .password(password)
                .riskLevel(riskLevel)
                .riskTimes(riskTimes)
                .build();
    }

    private static Args getArgsInteractive() {
        Scanner scanner = new Scanner(System.in);

        System.out.println("Welcome");
        System.out.println("Enter login url for *.fx.lan: ");
        System.out.println("e.g.:https://prd-wlt-dk-01.fx.lan:8443/rest/public/api/security/login");

        String loginUrl = scanner.next();

        System.out.println("Enter logout url for *.fx.lan: ");
        System.out.println("e.g.:https://prd-wlt-dk-01.fx.lan:8443/rest/api/security/logout");

        String logoutUrl = scanner.next();

        System.out.println("Enter url for *.fx.lan: ");
        System.out.println("e.g.:https://prd-wlt-dk-01.fx.lan:8443/crm-solution");

        String url = scanner.next();

        System.out.println("Enter username: ");
        System.out.println("e.g.:ameliathomas50050001");

        // get their input as a String
        String username = scanner.next();

        System.out.println("Enter password: ");
        System.out.println("e.g.:Passw0rd!");

        String password = scanner.next();

        System.out.println("Enter riskLevel: ");
        System.out.println("0 - Informational");
        System.out.println("1 - Low");
        System.out.println("2 - Medium");

        Integer riskLevel = Integer.parseInt(scanner.next());

        System.out.println("Enter occure times for risk level: ");
        System.out.println("Alerts with selected risk level more than X - println the report.");

        Integer riskTimes = Integer.parseInt(scanner.next());

        return Args.builder()
                .loginUrl(loginUrl)
                .logoutUrl(logoutUrl)
                .url(url)
                .loggedInIndicator("<a _ngcontent-c1=\"\" href=\"javascript:void(0)\" translate=\"\"> Logout </a>")
                .username(username)
                .password(password)
                .riskLevel(riskLevel)
                .riskTimes(riskTimes)
                .build();
    }

}
