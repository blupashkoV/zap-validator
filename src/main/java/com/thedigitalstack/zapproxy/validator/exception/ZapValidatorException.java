package com.thedigitalstack.zapproxy.validator.exception;

public class ZapValidatorException extends RuntimeException {
    public ZapValidatorException(String message) {
        super(message);
    }
}
