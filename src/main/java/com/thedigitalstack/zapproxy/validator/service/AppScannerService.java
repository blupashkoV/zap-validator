package com.thedigitalstack.zapproxy.validator.service;

import com.thedigitalstack.zapproxy.validator.model.Args;
import com.thedigitalstack.zapproxy.validator.model.ZapResponse;

public interface AppScannerService {

    ZapResponse getReport(Args args);
}
