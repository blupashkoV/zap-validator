package com.thedigitalstack.zapproxy.validator.service.impl;

import com.thedigitalstack.zapproxy.validator.model.Args;
import org.zaproxy.clientapi.core.ApiResponse;
import org.zaproxy.clientapi.core.ApiResponseElement;
import org.zaproxy.clientapi.core.ClientApi;
import org.zaproxy.clientapi.core.ClientApiException;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

public class FormAuth {
    private static final String contextId = "1";
    private static final String contextName = "Default Context";

    public static void setIncludeAndExcludeInContext(ClientApi clientApi, Args args) throws ClientApiException {
        clientApi.context.includeInContext(contextName, args.getUrl() + ".*");
        clientApi.context.excludeFromContext(contextName, args.getLogoutUrl());
    }


    public static void setLoggedInIndicator(ClientApi clientApi, String loggedInIndicator) throws ClientApiException {
        // Prepare values to set, with the logged in indicator as a regex matching the logout link

        // Actually set the logged in indicator
        clientApi.authentication.setLoggedInIndicator(contextId, java.util.regex.Pattern.quote(loggedInIndicator));

        // Check out the logged in indicator that is set
        System.out.println("Configured logged in indicator regex: "
                + ((ApiResponseElement) clientApi.authentication.getLoggedInIndicator(contextId)).getValue());
    }

    public static void setFormBasedAuthentication(ClientApi clientApi, Args args) throws ClientApiException,
            UnsupportedEncodingException {
        // Setup the authentication method

        String loginRequestData = "username=" + args.getUsername() + "&password=" + args.getPassword();

        // Prepare the configuration in a format similar to how URL parameters are formed. This
        // means that any value we add for the configuration values has to be URL encoded.
        StringBuilder formBasedConfig = new StringBuilder();
        formBasedConfig.append("loginUrl=").append(URLEncoder.encode(args.getLoginUrl(), "UTF-8"));
        formBasedConfig.append("&loginRequestData=").append(URLEncoder.encode(loginRequestData, "UTF-8"));

        System.out.println("Setting form based authentication configuration as: "
                + formBasedConfig.toString());
        clientApi.authentication.setAuthenticationMethod(contextId, "formBasedAuthentication",
                formBasedConfig.toString());

        // Check if everything is set up ok
        System.out
                .println("Authentication config: " + clientApi.authentication.getAuthenticationMethod(contextId).toString(0));
    }

    public static String setUserAuthConfig(ClientApi clientApi, Args args) throws ClientApiException, UnsupportedEncodingException {

        // Make sure we have at least one user
        String userId = extractUserId(clientApi.users.newUser(contextId, args.getUsername()));

        // Prepare the configuration in a format similar to how URL parameters are formed. This
        // means that any value we add for the configuration values has to be URL encoded.
        StringBuilder userAuthConfig = new StringBuilder();
        userAuthConfig.append("username=").append(URLEncoder.encode(args.getUsername(), "UTF-8"));
        userAuthConfig.append("&password=").append(URLEncoder.encode(args.getPassword(), "UTF-8"));

        System.out.println("Setting user authentication configuration as: " + userAuthConfig.toString());
        clientApi.users.setAuthenticationCredentials(contextId, userId, userAuthConfig.toString());
        clientApi.users.setUserEnabled(contextId, userId, "true");
        clientApi.forcedUser.setForcedUser(contextId, userId);
        clientApi.forcedUser.setForcedUserModeEnabled(true);

        // Check if everything is set up ok
        System.out.println("Authentication config: " + clientApi.users.getUserById(contextId, userId).toString(0));
        return userId;
    }

    public static String extractUserId(ApiResponse response) {
        return ((ApiResponseElement) response).getValue();
    }

    public static ApiResponse scanAsUser(ClientApi clientApi, String userId, String target) throws ClientApiException {
        return clientApi.spider.scanAsUser(contextId, userId, target, null, "true", null);
    }

}