package com.thedigitalstack.zapproxy.validator.service.impl;

import com.google.gson.Gson;
import com.thedigitalstack.zapproxy.validator.model.Args;
import com.thedigitalstack.zapproxy.validator.model.ZapResponse;
import com.thedigitalstack.zapproxy.validator.service.AppScannerService;
import org.zaproxy.clientapi.core.ApiResponse;
import org.zaproxy.clientapi.core.ApiResponseElement;
import org.zaproxy.clientapi.core.ClientApi;

import java.nio.charset.StandardCharsets;

import static com.thedigitalstack.zapproxy.validator.service.impl.FormAuth.scanAsUser;
import static com.thedigitalstack.zapproxy.validator.service.impl.FormAuth.setFormBasedAuthentication;
import static com.thedigitalstack.zapproxy.validator.service.impl.FormAuth.setIncludeAndExcludeInContext;
import static com.thedigitalstack.zapproxy.validator.service.impl.FormAuth.setLoggedInIndicator;
import static com.thedigitalstack.zapproxy.validator.service.impl.FormAuth.setUserAuthConfig;

public class ZapService implements AppScannerService {

    private static final String ZAP_ADDRESS = "localhost";
    private static final Integer ZAP_PORT = 8080;
    private static final String ZAP_API_KEY = "b46c24vhbm8pqfoclq4t4l8cjc"; // Change this if you have set the apikey in ZAP via Options / API

    public ZapResponse getReport(Args args) {
        final String url = args.getUrl();
        ClientApi api = new ClientApi(ZAP_ADDRESS, ZAP_PORT, ZAP_API_KEY);

        try {
            // Start spidering the target
            System.out.println("Spider : " + url);
            // It's not necessary to pass the ZAP API key again, already set when creating the
            // ClientApi.
            api.alert.deleteAllAlerts();


            setIncludeAndExcludeInContext(api, args);
            setFormBasedAuthentication(api, args);
            setLoggedInIndicator(api, args.getLoggedInIndicator());
            String userId = setUserAuthConfig(api, args);

            ApiResponse resp = scanAsUser(api, userId, url);
            String scanid;
            int progress;

            // The scan now returns a scan id to support concurrent scanning
            scanid = ((ApiResponseElement) resp).getValue();

            // Poll the status until it completes
            while (true) {
                Thread.sleep(1000);
                progress =
                        Integer.parseInt(
                                ((ApiResponseElement) api.spider.status(scanid)).getValue());
                System.out.println("Spider progress : " + progress + "%");
                if (progress >= 100) {
                    break;
                }
            }
            System.out.println("Spider complete");

            // Poll the number of records the passive scanner still has to scan until it completes
            while (true) {
                Thread.sleep(1000);
                progress =
                        Integer.parseInt(
                                ((ApiResponseElement) api.pscan.recordsToScan()).getValue());
                System.out.println("Passive Scan progress : " + progress + " records left");
                if (progress < 1) {
                    break;
                }
            }
            System.out.println("Passive Scan complete");

            System.out.println("Active scan : " + url);
            resp = api.ascan.scan(url, "True", "False", null, null, null);

            // The scan now returns a scan id to support concurrent scanning
            scanid = ((ApiResponseElement) resp).getValue();

            // Poll the status until it completes
            while (true) {
                Thread.sleep(5000);
                progress =
                        Integer.parseInt(
                                ((ApiResponseElement) api.ascan.status(scanid)).getValue());
                System.out.println("Active Scan progress : " + progress + "%");
                if (progress >= 100) {
                    break;
                }
            }
            System.out.println("Active Scan complete");

            System.out.println("Alerts:");
            String response = new String(api.core.jsonreport(), StandardCharsets.UTF_8);

            System.out.println(response);
            System.out.println();

            return responseToReport(response);

        } catch (Exception e) {
            System.out.println("Exception : " + e.getMessage());
            e.printStackTrace();
        }
        return null;
    }

    private ZapResponse responseToReport(String response) {
        Gson gson = new Gson();

        return gson.fromJson(response, ZapResponse.class);

    }

}
