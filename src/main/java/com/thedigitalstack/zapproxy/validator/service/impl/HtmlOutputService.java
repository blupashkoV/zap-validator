package com.thedigitalstack.zapproxy.validator.service.impl;

import com.thedigitalstack.zapproxy.validator.model.AlertsItem;
import com.thedigitalstack.zapproxy.validator.model.Args;
import com.thedigitalstack.zapproxy.validator.model.SiteItem;
import com.thedigitalstack.zapproxy.validator.model.ZapResponse;
import com.thedigitalstack.zapproxy.validator.service.OutputService;
import org.apache.velocity.Template;
import org.apache.velocity.VelocityContext;
import org.apache.velocity.app.VelocityEngine;
import org.apache.velocity.runtime.RuntimeConstants;
import org.apache.velocity.runtime.resource.loader.ClasspathResourceLoader;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.time.LocalDate;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

public class HtmlOutputService implements OutputService {

    public void print(Args args, ZapResponse response) {

        SiteItem siteItem = response.getSite().stream()
                .filter(site -> site.getAlerts() != null)
                .filter(site -> !site.getAlerts().isEmpty())
                .findAny()
                .get();

        List<AlertsItem> hiRiskLevelReports = siteItem.getAlerts().stream()
                .filter(report -> Integer.parseInt(report.getRiskcode()) >= args.getRiskLevel())
                .collect(Collectors.toList());

        if (args.getRiskTimes() < hiRiskLevelReports.size()) {
            createHtml(siteItem, hiRiskLevelReports);
        } else {
            System.out.println("It's ok.");
        }

    }

    public void createHtml(SiteItem site, List<AlertsItem> alertsItems) {

        VelocityEngine velocityEngine = new VelocityEngine();
        velocityEngine.setProperty(RuntimeConstants.RESOURCE_LOADER, "classpath");
        velocityEngine.setProperty("classpath.resource.loader.class", ClasspathResourceLoader.class.getName());
        velocityEngine.init();
        Template template = velocityEngine.getTemplate("index.vm");

        VelocityContext context = new VelocityContext();

        Long timestamp = new Date().getTime();

        LocalDate date = LocalDate.now();
        String name = "ZAP Report " + date + " - ID_" + timestamp;
        context.put("name", name);
        context.put("host", site.getHost());
        context.put("ssl", site.getSsl());
        context.put("alerts", alertsItems);
        StringWriter writer = new StringWriter();
        template.merge(context, writer);

        try {
            PrintWriter out = new PrintWriter(new BufferedWriter(new FileWriter(timestamp + ".html", true)));
            out.println(writer);
            out.close();
        } catch (IOException e) {
        }
    }

}
