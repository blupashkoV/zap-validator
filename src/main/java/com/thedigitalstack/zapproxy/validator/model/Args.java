package com.thedigitalstack.zapproxy.validator.model;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class Args {
    private String loginUrl;
    private String logoutUrl;
    private String url;
    private String loggedInIndicator;
    private String username;
    private String password;
    private Integer riskLevel;
    private Integer riskTimes;
}
