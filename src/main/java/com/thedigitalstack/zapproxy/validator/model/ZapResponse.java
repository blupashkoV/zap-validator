package com.thedigitalstack.zapproxy.validator.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ZapResponse {
    private List<SiteItem> site;
    private String version;
    private String generated;
}