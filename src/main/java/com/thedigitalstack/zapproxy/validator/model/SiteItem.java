package com.thedigitalstack.zapproxy.validator.model;

import com.google.gson.annotations.SerializedName;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class SiteItem {
    private List<AlertsItem> alerts;
    @SerializedName(value = "@name")
    private String name;
    @SerializedName(value = "@ssl")
    private String ssl;
    @SerializedName(value = "@port")
    private String port;
    @SerializedName(value = "@host")
    private String host;
}