package com.thedigitalstack.zapproxy.validator.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class AlertsItem{
	private String sourceid;
	private String riskdesc;
	private String method;
	private String pluginid;
	private String cweid;
	private String confidence;
	private String wascid;
	private String uri;
	private String reference;
	private String solution;
	private String alert;
	private String param;
	private String name;
	private String riskcode;
	private String alertRef;
	private String desc;
	private String evidence;
	private String otherinfo;
}
