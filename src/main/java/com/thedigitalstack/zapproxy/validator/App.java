package com.thedigitalstack.zapproxy.validator;

import com.thedigitalstack.zapproxy.validator.model.Args;
import com.thedigitalstack.zapproxy.validator.model.ZapResponse;
import com.thedigitalstack.zapproxy.validator.service.AppScannerService;
import com.thedigitalstack.zapproxy.validator.service.OutputService;
import com.thedigitalstack.zapproxy.validator.service.impl.HtmlOutputService;
import com.thedigitalstack.zapproxy.validator.service.impl.ZapService;
import com.thedigitalstack.zapproxy.validator.util.ArgInitializer;

public class App {

    public static void main(String[] args) {
        AppScannerService scanner = new ZapService();
        OutputService outputService = new HtmlOutputService();

        Args cliArgs = ArgInitializer.validateAndInitializeArgs(args);

        ZapResponse response = scanner.getReport(cliArgs);

        outputService.print(cliArgs, response);

    }

}

